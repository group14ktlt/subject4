Bài 1: InfixToPrefix 
Người dùng nhập vào biểu thức Infix gồm các toán hạng A,B,C,D ... (phân biệt in thường và in hoa)
và các toán tử +,-,*,/ và các dấu ( ) ngăn cách nhau bởi space
Nếu biểu thức nhập sai ý nghĩa toán học hoặc có các kí tự đặc biệt thì chương trình sẽ báo lỗi

Bài 2: PrefixToInfix
Người dùng nhập vào các biểu thức dưới dạng prefix gồm các toán hạng  A,B,C,D ... (phân biệt in thường và in hoa)
và các toán tử +,-,*,/ ngăn cách nhau bởi dấu space
Nếu biểu thức nhập sai ý nghĩa toán học hoặc có các kí tự đặc biệt thì chương trình sẽ báo lỗi

Bài 3: CalculateInfix
Người dùng nhập vào biểu thức dưới dạng infix bao gồm các số nguyên và các toán tử +, - ,*, /
Chương trình sẽ trả về biểu thức dưới dạng prefix và kết quả của biểu thức đó.
Chương trình sẽ báo lỗi khi nhập các kí tự đặc biệt hoặc thực hiện các phép toán sai
ý nghĩa toán học.